// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PCGAIGame : ModuleRules
{
	public PCGAIGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
