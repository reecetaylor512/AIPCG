// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PCGAIGameGameMode.generated.h"

UCLASS(minimalapi)
class APCGAIGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APCGAIGameGameMode();
};



